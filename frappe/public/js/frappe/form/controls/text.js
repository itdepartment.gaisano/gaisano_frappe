frappe.ui.form.ControlText = frappe.ui.form.ControlData.extend({
	html_element: "textarea",
	horizontal: false,
	make_wrapper: function () {
		this._super();
		this.$wrapper.find(".like-disabled-input").addClass("for-description");
	},
	make_input: function () {
		this._super();

		console.log(this)

		//Use 30px for List View
		let url = this.$wrapper[0].baseURI
		if (url.substr(url.length - 4 )  == "List" || url.substr(url.length - 6 ) == "Report") {
			this.$input.css({ 'height': '30px' })
		} else {
			this.$input.css({ 'height': '300px' })
		}

	}
});

frappe.ui.form.ControlLongText = frappe.ui.form.ControlText;
frappe.ui.form.ControlSmallText = frappe.ui.form.ControlText.extend({
	make_input: function () {
		this._super();
		this.$input.css({ 'height': '150px' });
	}
});
